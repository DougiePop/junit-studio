package training;

public class RomanNumeral {

//    public static void main(String[] args) {
//        final Numeral[] values = Numeral.values();
//        System.out.println(values);
//        for (Numeral value : values){
//            System.out.println(value.value);
//        }
//    }



    enum Numeral {
        I(1), IV(4), V(5), IX(9), X(10), XL(40), L(50), XC(90), C(100), CD(400), D(500), CM(900), M(1000);
        int value;

        Numeral(int value) {
            this.value = value;
        }
    }

    /**
     * Convert positive integers to roman numerals
     *
     * Examples: 4 -> IV, 51 -> LI, 999 -> CMXCIX, 49 -> XLIX
     *
     * @param n - positive integer
     * @return Roman numeral string version of integer
     */

    // in roman numeral, there are special cases for all digits with 4 & 9 in it.

    public static String fromInt (int n) {

        if(n <= 0) {
            throw new IllegalArgumentException();
        }

        StringBuilder buf = new StringBuilder();

        final Numeral[] values = Numeral.values();
        for (int i = values.length - 1; i >= 0; i--) {
            while (n >= values[i].value) {
                buf.append(values[i]);
                n -= values[i].value;
            }
        }
        return buf.toString();
    }

}
