package launchcode;

import org.junit.Test;

import training.BalancedBrackets;
import training.BinarySearch;
import training.RomanNumeral;
import training.Fraction;

import static org.junit.Assert.*;

public class BalancedBracketsTest {

    //TODO: add tests here
    @Test
    public void isBalancedBracketsPartOne(){
        boolean bal = BalancedBrackets.hasBalancedBrackets("LaunchCode");
        assertEquals(true, bal);
    };

    @Test
    public void isBalancedBracketsPartTwo(){
        boolean bal = BalancedBrackets.hasBalancedBrackets("");
        assertEquals(true, bal);
    };

    @Test
    public void isBalancedBracketsPartThree(){
        boolean bal = BalancedBrackets.hasBalancedBrackets("[LaunchCode");
        assertEquals(false, bal);
    };

    @Test
    public void isBalancedBracketsPartFour(){
        boolean bal = BalancedBrackets.hasBalancedBrackets("Launch]Code[");
        assertEquals(false, bal);
    };

    @Test
    public void isBalancedBracketsPartFive(){
        boolean bal = BalancedBrackets.hasBalancedBrackets("Launch]Code[][[");
        assertEquals(false, bal);
    };

    @Test
    public void isBinarySearchPartOne(){
        int result = BinarySearch.binarySearch(new int[]{4,5,6,7,8}, 7);
        assertEquals(3, result);
    };

    @Test
    public void isBinarySearchPartTwo(){
        int result = BinarySearch.binarySearch(new int[]{4,5,6,6,8}, 6);
        assertEquals(2, result);
    };

    @Test
    public void isBinarySearchPartThree(){
        int result = BinarySearch.binarySearch(new int[]{4,5,6,6,7,8}, 7);
        assertEquals(4, result);
    };

    @Test
    public void isBinarySearchPartFour(){
        int result = BinarySearch.binarySearch(new int[]{4,5,6}, 8);
        assertEquals(-1, result);
    };

    @Test
    public void isBinarySearchPartFive(){
        int result = BinarySearch.binarySearch(new int[]{}, 4);
        assertEquals(-1, result);
    };

    @Test
    public void isBinarySearchPartSix(){
        int result = BinarySearch.binarySearch(new int[]{1,2,3,4,5,6,7,8,9,10}, 2);
        assertEquals(1, result);
    };

    @Test
    public void isRomanNumeralPartOne(){
        String romanNum = RomanNumeral.fromInt(4);
        assertEquals("IV", romanNum);
    };

    @Test
    public void isRomanNumeralPartTwo(){
        String romanNum = RomanNumeral.fromInt(999);
        assertEquals("CMXCIX", romanNum);
    };

    @Test
    public void isRomanNumeralPartThree(){
        String romanNum = RomanNumeral.fromInt(51);
        assertEquals("LI", romanNum);
    };

    @Test
    public void isRomanNumeralPartFour(){
        String romanNum = RomanNumeral.fromInt(49);
        assertEquals("XLIX", romanNum);
    };

    @Test
    public void isRomanNumeralPartFive(){
        String romanNum = RomanNumeral.fromInt(450);
        assertEquals("CDL", romanNum);
    };

    @Test
    public void isFractionPartOne(){
        Fraction half = new Fraction(1, 2);
        Boolean result = half.equals(new Fraction(1,2));
        assertEquals(true, result);
    };

    @Test
    public void isFractionPartTwo(){
        Fraction half = new Fraction(2, 4);
        Boolean result = half.equals(new Fraction(1,2));
        assertEquals(true, result);
    };

    @Test
    public void isFractionPartThree(){
        Fraction half = new Fraction(2, 4);
        Boolean result = half.equals(new Fraction(1,2));
        assertEquals(true, result);
    };

    @Test
    public void isFractionPartFour(){
        Fraction half = new Fraction(2, 4);
        int result = half.compareTo(new Fraction(1,2));
        assertEquals(0, result);
    };

    @Test
    public void isFractionPartFive(){
        Fraction frac = new Fraction(3, 4);
        int result = frac.compareTo(new Fraction(1,2));
        assertEquals(1, result);
    };

    @Test
    public void isFractionPartSix(){
        Fraction frac = new Fraction(1, 2);
        int result = frac.compareTo(new Fraction(4,5));
        assertEquals(-1, result);
    };




}
